﻿using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class Author
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public bool IsDeleted { get; set; }
    public List<Book> Books { get; set; }

    public Author()
    {
      Books = new List<Book>();
    }
  }
}
