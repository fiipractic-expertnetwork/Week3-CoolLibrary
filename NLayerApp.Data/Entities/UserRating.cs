﻿namespace NLayerApp.Data.Entities
{
  public class UserRating
  {
    public int Id { get; set; }
    public int UserId { get; set; }
    public string Text { get; set; }

    public decimal? NumberOfStars { get; set; }
    public User User { get; set; }
  }
}
