﻿using System;
using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class User
  {
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public DateTime CreatedOn { get; set; }
    public bool IsFemale { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    public string Address { get; set; }
    public string PhoneNumber { get; set; }
    public string ProfilePicture { get; set; }
        public int? CountyId { get; set; }

    public County County { get; set; }
    public List<UserRating> UserRatings { get; set; }
    public List<UserBook> UserBooks { get; set; }
    public List<ChatMessage> ChatMessages { get; set; }
    public List<ChatUser> ChatUsers { get; set; }
    public List<Rental> Borrowings { get; set; }
    public List<Rental> Lendings { get; set; }
    public List<Wishlist> Wishlists { get; set; }

    public User()
    {
      UserRatings = new List<UserRating>();
      UserBooks = new List<UserBook>();
      ChatMessages = new List<ChatMessage>();
      ChatUsers = new List<ChatUser>();
      Wishlists = new List<Wishlist>();
    }
  }
}
