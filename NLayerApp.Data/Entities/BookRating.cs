﻿namespace NLayerApp.Data.Entities
{
  public class BookRating
  {
    public int Id { get; set; }
    public int BookId { get; set; }
    public bool IsDeleted { get; set; }
    public string ShortDescription { get; set; }
    public string LongDescription { get; set; }
    public Book Book { get; set; }
    public decimal? NumberOfStars { get; set; }
  }
}
