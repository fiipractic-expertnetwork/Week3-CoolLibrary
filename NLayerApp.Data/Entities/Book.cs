﻿using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class Book
  {
    public int Id { get; set; }
    public int CategoryId { get; set; }
    public bool IsDeleted { get; set; }
    public Category Category { get; set; }
    public int AuthorId { get; set; }
    public Author Author { get; set; }
    public string Title { get; set; }
    public List<BookRating> BookRatings { get; set; }
    public List<UserBook> UserBooks { get; set; }
    public List<BookWishlist> BookWishlists { get; set; }

    public Book()
    {
      BookRatings = new List<BookRating>();
      UserBooks = new List<UserBook>();
      BookWishlists = new List<BookWishlist>();
    }
  }
}
