﻿using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class Category
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public bool IsDeleted { get; set; }
    public List<Book> Books { get; set; }

    public Category()
    {
      Books = new List<Book>();
    }
  }
}
