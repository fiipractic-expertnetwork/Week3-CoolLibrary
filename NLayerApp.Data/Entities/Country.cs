﻿using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class Country
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public List<County> Counties { get; set; }

    public Country()
    {
      Counties = new List<County>();
    }
  }
}
