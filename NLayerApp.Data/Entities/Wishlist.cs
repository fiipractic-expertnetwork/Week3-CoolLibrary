﻿using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class Wishlist
  {
    public int Id { get; set; }
    public int UserId { get; set; }
    public string Name { get; set; }
    public bool IsDeleted { get; set; }
    public User User { get; set; }
    public List<BookWishlist> BookWishlists { get; set; }

    public Wishlist()
    {
      BookWishlists = new List<BookWishlist>();
    }
  }
}
