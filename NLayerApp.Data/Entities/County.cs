﻿using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class County
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public int CountryID { get; set; }
    public Country Country { get; set; }
    public List<User> Users { get; set; }

    public County()
    {
      Users = new List<User>();
    }
  }
}
