﻿namespace NLayerApp.Data.Entities
{
  public class Rental
  {
    public int Id { get; set; }
    public int RentedBy { get; set; }
    public int RentedTo { get; set; }
    public User Lender { get; set; }
    public User Borrower { get; set; }
  }
}
