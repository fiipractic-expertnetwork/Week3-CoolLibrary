﻿using System;
using System.Collections.Generic;

namespace NLayerApp.Data.Entities
{
  public class Chat
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime? ClosedOn { get; set; }
    public List<ChatMessage> ChatMessages { get; set; }
    public List<ChatUser> ChatUsers { get; set; }
    public Chat()
    {
      ChatMessages = new List<ChatMessage>();
      ChatUsers = new List<ChatUser>();
    }
  }
}
