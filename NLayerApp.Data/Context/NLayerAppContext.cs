﻿using Microsoft.EntityFrameworkCore;
using NLayerApp.Data.Entities;

namespace NLayerApp.Data.Context
{
  public class NLayerAppContext : DbContext
  {
    public DbSet<Author> Authors { get; set; }
    public DbSet<Book> Books { get; set; }
    public DbSet<BookRating> BookRatings { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Chat> Chats { get; set; }
    public DbSet<ChatMessage> ChatMessages { get; set; }
    public DbSet<Country> Countries { get; set; }
    public DbSet<County> Counties { get; set; }
    public DbSet<Rental> Rentals { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<UserRating> UserRatings { get; set; }
    public DbSet<Wishlist> Wishlist { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
            optionsBuilder.UseSqlServer("Data Source=office.expertnetwork.ro,14334;Initial Catalog=CoolLibrary;User Id=fiipractic1;Password=jYnLUUBALrbI4vsSVh7l;");
    }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Author>(x =>
      {
        x.Property(p => p.Name).HasColumnType("nvarchar(100)");
        x.Property(p => p.Description).HasColumnType("nvarchar(1000)");
      });

      modelBuilder.Entity<Book>(x =>
      {
        x.Property(p => p.Title).HasColumnType("nvarchar(200)");
      });

      modelBuilder.Entity<Book>().HasOne(x => x.Author).WithMany(x => x.Books).HasForeignKey(x => x.AuthorId);
      modelBuilder.Entity<Book>().HasOne(x => x.Category).WithMany(x => x.Books).HasForeignKey(x => x.CategoryId);

      modelBuilder.Entity<BookRating>(x =>
      {
        x.Property(p => p.ShortDescription).HasColumnType("nvarchar(200)");
        x.Property(p => p.LongDescription).HasColumnType("nvarchar(MAX)");
        x.Property(p => p.NumberOfStars).IsRequired(false);
      });

      modelBuilder.Entity<BookRating>().HasOne(x => x.Book).WithMany(x => x.BookRatings).HasForeignKey(x => x.BookId);

      modelBuilder.Entity<BookWishlist>().HasKey(x => new { x.BookId, x.WishlistId });
      modelBuilder.Entity<BookWishlist>().HasOne(x => x.Book).WithMany(x => x.BookWishlists).HasForeignKey(x => x.BookId);
      modelBuilder.Entity<BookWishlist>().HasOne(x => x.Wishlist).WithMany(x => x.BookWishlists).HasForeignKey(x => x.WishlistId);

      modelBuilder.Entity<Chat>(x =>
      {
        x.Property(p => p.Name).HasColumnType("nvarchar(150)");
        x.Property(p => p.CreatedOn).HasColumnType("datetime2").IsRequired();
        x.Property(p => p.ClosedOn).HasColumnType("datetime2").IsRequired(false);
      });

      modelBuilder.Entity<ChatUser>().HasKey(x => new { x.ChatId, x.UserId });
      modelBuilder.Entity<ChatUser>().HasOne(x => x.Chat).WithMany(x => x.ChatUsers).HasForeignKey(x => x.ChatId);
      modelBuilder.Entity<ChatUser>().HasOne(x => x.User).WithMany(x => x.ChatUsers).HasForeignKey(x => x.UserId);

      modelBuilder.Entity<ChatMessage>(x =>
      {
        x.Property(p => p.Content).HasColumnType("nvarchar(MAX)");
      });

      modelBuilder.Entity<ChatMessage>().HasOne(x => x.Chat).WithMany(x => x.ChatMessages).HasForeignKey(x => x.ChatId);
      modelBuilder.Entity<ChatMessage>().HasOne(x => x.User).WithMany(x => x.ChatMessages).HasForeignKey(x => x.UserId);

      modelBuilder.Entity<Country>(x =>
      {
        x.Property(p => p.Name).HasColumnType("nvarchar(30)");
      });

      modelBuilder.Entity<Country>().HasMany(x => x.Counties).WithOne(x => x.Country).HasForeignKey(x => x.CountryID);

      modelBuilder.Entity<Rental>().HasOne(x => x.Lender).WithMany(x => x.Lendings).OnDelete(DeleteBehavior.Restrict).HasForeignKey(x => x.RentedBy);
      modelBuilder.Entity<Rental>().HasOne(x => x.Borrower).WithMany(x => x.Borrowings).OnDelete(DeleteBehavior.Restrict).HasForeignKey(x => x.RentedTo);

      modelBuilder.Entity<User>(x =>
      {
        x.Property(p => p.FirstName).HasColumnType("nvarchar(30)");
        x.Property(p => p.LastName).HasColumnType("nvarchar(30)");
        x.Property(p => p.Email).HasColumnType("nvarchar(50)");
        x.Property(p => p.Password).HasColumnType("varchar(64)");
        x.Property(p => p.CreatedOn).HasColumnType("datetime2");
        x.Property(p => p.Address).HasColumnType("varchar(200)");
        x.Property(p => p.PhoneNumber).HasColumnType("varchar(20)");
        x.Property(p => p.ProfilePicture).HasColumnType("varchar(200)");
      });

      modelBuilder.Entity<User>().HasOne(x => x.County).WithMany(x => x.Users).HasForeignKey(x => x.CountyId);
      modelBuilder.Entity<User>().HasMany(x => x.Wishlists).WithOne(x => x.User).HasForeignKey(x => x.UserId);

      modelBuilder.Entity<UserBook>().HasKey(x => new { x.UserId, x.BookId });
      modelBuilder.Entity<UserBook>().HasOne(x => x.User).WithMany(x => x.UserBooks).HasForeignKey(x => x.UserId);
      modelBuilder.Entity<UserBook>().HasOne(x => x.Book).WithMany(x => x.UserBooks).HasForeignKey(x => x.BookId);

      modelBuilder.Entity<UserRating>(x =>
      {
        x.Property(p => p.Text).HasColumnType("nvarchar(MAX)");
        x.Property(p => p.NumberOfStars).HasColumnType("decimal(3,2)").IsRequired(false);
      });

      modelBuilder.Entity<UserRating>().HasOne(x => x.User).WithMany(x => x.UserRatings).HasForeignKey(x => x.UserId);

      modelBuilder.Entity<Wishlist>(x =>
      {
        x.Property(p => p.Name).HasColumnType("nvarchar(40)");
      });
    }
  }
}
