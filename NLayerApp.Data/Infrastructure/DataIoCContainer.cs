﻿using Microsoft.Extensions.DependencyInjection;
using NLayerApp.Data.Context;

namespace NLayerApp.Data.Infrastructure
{
  public static class DataIoCContainer
  {
    public static IServiceCollection AddToContainer(this IServiceCollection services)
    {
      services.AddDbContext<NLayerAppContext>();
      services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
      services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));

      return services;
    }
  }
}
