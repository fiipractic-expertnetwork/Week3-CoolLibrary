﻿namespace NLayerApp.Data.Infrastructure
{
  public interface IUnitOfWork
  {
    void Commit();
  }
}
