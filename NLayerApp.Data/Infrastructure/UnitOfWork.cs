﻿using Microsoft.EntityFrameworkCore;
using NLayerApp.Data.Context;

namespace NLayerApp.Data.Infrastructure
{
  public class UnitOfWork : IUnitOfWork
  {
    private NLayerAppContext dbContext;

    public UnitOfWork(NLayerAppContext dbContext)
    {
      this.dbContext = dbContext;
    }

    public void Commit()
    {
      dbContext.SaveChanges();
    }
  }
}
