﻿namespace NLayerApp.Services.Common.Books.Dto
{
  public class BookDto
  {
    public int Id { get; set; }
    public int CategoryId { get; set; }
    public bool IsDeleted { get; set; }
    public int AuthorId { get; set; }
    public string Title { get; set; }
  }
}
