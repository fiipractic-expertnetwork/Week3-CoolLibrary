﻿namespace NLayerApp.Services.Common.Books.Dto
{
  public class DetailedBookDto : BookDto
  {
    public string Category { get; set; }
    public string Author { get; set; }
  }
}
