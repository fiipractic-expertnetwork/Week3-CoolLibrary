﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NLayerApp.Data.Entities;
using NLayerApp.Data.Infrastructure;
using NLayerApp.Services.Common.Books.Dto;
using Omu.ValueInjecter;

namespace NLayerApp.Services.Common.Books
{
  public class BookService : IBookService
  {
    private readonly IRepository<Book> bookRepository;
    private readonly IUnitOfWork unitOfWork;

    public BookService(IRepository<Book> bookRepository, IUnitOfWork unitOfWork)
    {
      this.bookRepository = bookRepository;
      this.unitOfWork = unitOfWork;
    }


    public BookDto GetBookById(int id)
    {
      if (id < 1) throw new ArgumentException(nameof(id));

      var bookEntity = bookRepository.Query(x => x.Id == id).FirstOrDefault();
      if (bookEntity == null) return null;

      var bookDto = new BookDto();
      bookDto.InjectFrom(bookEntity);
      //InjectFrom is from Omu.ValueInjecter library
      //Can do mapping between two objects having the same field types and names.

      return bookDto;
      // we can't return entities to Web layer, so we return a DTO. 
      // https://docs.microsoft.com/en-us/aspnet/web-api/overview/data/using-web-api-with-entity-framework/part-5
    }

    public void CreateBook(BookDto bookDto)
    {
      if (bookDto == null) throw new ArgumentNullException(nameof(bookDto)); // We should never call Create giving as parameter a null object

      var bookEntity = new Book();
      bookEntity.InjectFrom(bookDto);

      bookRepository.Add(bookEntity);
      unitOfWork.Commit();
    }

    public List<DetailedBookDto> GetDetailedBooks()
    {
      var bookEntities = bookRepository.Query().Include(x => x.Author).Include(x => x.Category).ToList();

      var detailedBookDtos = new List<DetailedBookDto>();
      foreach (var bookEntity in bookEntities)
      {
        var detailedBookDto = new DetailedBookDto
        {
          Author = bookEntity.Author.Name,
          AuthorId = bookEntity.AuthorId,
          Category = bookEntity.Category.Name,
          CategoryId = bookEntity.CategoryId,
          Id = bookEntity.Id,
          IsDeleted = bookEntity.IsDeleted,
          Title = bookEntity.Title
        };
        detailedBookDtos.Add(detailedBookDto);
      }

      return detailedBookDtos;
    }

    public void UpdateBook(BookDto bookDto)
    {
      if (bookDto == null) throw new ArgumentNullException(nameof(bookDto));

      var bookEntity = bookRepository.Query(x => x.Id == bookDto.Id).FirstOrDefault();
      if (bookEntity == null) throw new Exception($"Cannot update book with id = {bookDto.Id}"); //Should be Custom Exception;

      bookEntity.InjectFrom(bookDto);
      bookRepository.Update(bookEntity);

      unitOfWork.Commit();
    }
  }
}
