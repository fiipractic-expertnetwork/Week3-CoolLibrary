﻿using System.Collections.Generic;
using NLayerApp.Services.Common.Books.Dto;

namespace NLayerApp.Services.Common.Books
{
  public interface IBookService
  {
    BookDto GetBookById(int id);
    void CreateBook(BookDto bookDto);
    List<DetailedBookDto> GetDetailedBooks();
    void UpdateBook(BookDto bookDto);
  }
}
