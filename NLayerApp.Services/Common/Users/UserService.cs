﻿using NLayerApp.Data.Entities;
using NLayerApp.Data.Infrastructure;
using NLayerApp.Services.Common.Users.Dto;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLayerApp.Services.Common.Users
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> userRepository;
        private readonly IRepository<Country> countryRepository;
        private readonly IUnitOfWork unitOfWork;

        public UserService(IRepository<User> userRepository, IUnitOfWork unitOfWork, IRepository<Country> countryRepository)
        {
            this.userRepository = userRepository;
            this.unitOfWork = unitOfWork;
            this.countryRepository = countryRepository;
        }

        public UserDto Login(string email, string password)
        {
            var user = userRepository.Query().FirstOrDefault(x => x.Email == email && x.Password == password);
            if (user == null) return null;
            var userDto = new UserDto();
            userDto.InjectFrom(user);
            return userDto;
        }

        public UserDto Register(string email, string password, string profilePicture)
        {
            if (userRepository.Query().Any(x => x.Email == email && x.IsDeleted == false))
                return null; //already exists an user with this email
            var user = new User
            {
                Email = email,
                Password = password,
                CreatedOn = DateTime.Now,
                ProfilePicture = profilePicture
            };
            userRepository.Add(user);
            unitOfWork.Commit();
            var userDto = new UserDto();
            userDto.InjectFrom(user);
            return userDto;
        }
    }
}
