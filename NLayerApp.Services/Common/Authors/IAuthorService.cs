﻿using System.Collections.Generic;
using NLayerApp.Services.Common.Authors.Dto;

namespace NLayerApp.Services.Common.Authors
{
  public interface IAuthorService
  {
    AuthorDto GetAuthorById(int id);
    List<AuthorDto> GetNonDeletedAuthors();
  }
}
