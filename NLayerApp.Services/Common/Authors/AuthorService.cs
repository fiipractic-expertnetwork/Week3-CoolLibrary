﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLayerApp.Data.Entities;
using NLayerApp.Data.Infrastructure;
using NLayerApp.Services.Common.Authors.Dto;
using Omu.ValueInjecter;

namespace NLayerApp.Services.Common.Authors
{
  public class AuthorService : IAuthorService
  {
    private readonly IRepository<Author> authorRepository;

    public AuthorService(IRepository<Author> authorRepository)
    {
      this.authorRepository = authorRepository;
    }

    public AuthorDto GetAuthorById(int id)
    {
      if (id < 1) throw new ArgumentException(nameof(id));

      var authorEntity = authorRepository.Query(x => x.Id == id).FirstOrDefault();
      if (authorEntity == null) return null;

      var authorDto = new AuthorDto();
      authorDto.InjectFrom(authorEntity);

      return authorDto;
    }

    public List<AuthorDto> GetNonDeletedAuthors()
    {
      var authorEntities = authorRepository.Query(x => !x.IsDeleted).ToList();

      var authorDtos = new List<AuthorDto>();
      foreach (var authorEntity in authorEntities)
      {
        var authorDto = new AuthorDto();
        authorDto.InjectFrom(authorEntity);
        authorDtos.Add(authorDto);
      }

      return authorDtos;
    }
  }
}
