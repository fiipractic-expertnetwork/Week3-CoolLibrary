﻿namespace NLayerApp.Services.Common.Authors.Dto
{
  public class AuthorDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public bool IsDeleted { get; set; }
  }
}
