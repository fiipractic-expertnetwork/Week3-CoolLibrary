﻿using System;
using NLayerApp.Data.Entities;
using NLayerApp.Data.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using NLayerApp.Services.Common.Categories.Dto;
using Omu.ValueInjecter;

namespace NLayerApp.Services.Common.Categories
{
  public class CategoryService : ICategoryService
  {
    private readonly IRepository<Category> categoryRepository;

    public CategoryService(IRepository<Category> categoryRepository)
    {
      this.categoryRepository = categoryRepository;
    }

    public CategoryDto GetCategoryById(int id)
    {
      if (id < 1) throw new ArgumentException(nameof(id));

      var categoryEntity = categoryRepository.Query(x => x.Id == id).FirstOrDefault();
      if (categoryEntity == null) return null;

      var categoryDto = new CategoryDto();
      categoryDto.InjectFrom(categoryEntity);

      return categoryDto;
    }

    public List<CategoryDto> GetNonDeletedCategories()
    {
      var categoryEntities = categoryRepository.Query(x => !x.IsDeleted).ToList();

      var categoryDtos = new List<CategoryDto>();
      foreach (var categoryEntity in categoryEntities)
      {
        var categoryDto = new CategoryDto();
        categoryDto.InjectFrom(categoryEntity);
        categoryDtos.Add(categoryDto);
      }

      return categoryDtos;
    }
  }
}
