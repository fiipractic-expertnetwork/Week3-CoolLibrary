﻿using System.Collections.Generic;
using NLayerApp.Services.Common.Categories.Dto;

namespace NLayerApp.Services.Common.Categories
{
  public interface ICategoryService
  {
    CategoryDto GetCategoryById(int id);
    List<CategoryDto> GetNonDeletedCategories();
  }
}
