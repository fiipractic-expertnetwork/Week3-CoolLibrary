﻿using System.Collections.Generic;
using NLayerApp.Services.Common.Countries.Dto;

namespace NLayerApp.Services.Common.Countries
{
    public interface ICountryService
    {
        List<CountryDto> GetAllCountries();
    }
}