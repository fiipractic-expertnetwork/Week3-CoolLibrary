﻿using NLayerApp.Data.Entities;
using NLayerApp.Data.Infrastructure;
using NLayerApp.Services.Common.Countries.Dto;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Text;

namespace NLayerApp.Services.Common.Countries
{
    public class CountryService : ICountryService
    {
        private readonly IRepository<Country> countryRepository;

        public CountryService(IRepository<Country> countryRepository)
        {
            this.countryRepository = countryRepository;
        }

        public List<CountryDto> GetAllCountries()
        {
            var countriesList = new List<CountryDto>();
            var dbCountries = countryRepository.GetAll();
            foreach (var dbCountry in dbCountries)
            {
                countriesList.Add((CountryDto)new CountryDto().InjectFrom(dbCountry));
            }
            return countriesList;
        }
    }
}
