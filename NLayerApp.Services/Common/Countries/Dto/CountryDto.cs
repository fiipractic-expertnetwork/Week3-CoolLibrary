﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NLayerApp.Services.Common.Countries.Dto
{
    public class CountryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
