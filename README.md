#  NLayerApp

### Prerequisites

.NET Core 2.0

Visual Studio 2015/2017

EF Core 2.0

## Authors

* **Ciubotariu Florin-Constantin** - (ciubotariu.florin@gmail.com)
* **Moniry-Abyaneh Daniel** - (daniel.moniry@gmail.com)