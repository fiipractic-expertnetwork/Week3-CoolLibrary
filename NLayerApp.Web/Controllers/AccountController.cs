﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NLayerApp.Services.Common.Users;
using NLayerApp.Web.Models.Account;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using NLayerApp.Services.Common.Countries;
using NLayerApp.Web.Models.Shared;
using Omu.ValueInjecter;
using System.IO;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace NLayerApp.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userService;
        private readonly ICountryService countryService;
        private readonly IHostingEnvironment hostingEnvironment;

        public AccountController(IUserService userService, ICountryService countryService, IHostingEnvironment hostingEnvironment)
        {
            this.userService = userService;
            this.countryService = countryService;
            this.hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            //Check the model state is not tempered with
            if (ModelState.IsValid)
            {
                //Login logic
                var user = userService.Login(model.Email, model.Password);
                if (user != null)
                {
                    LoginMethod(model.Email, model.RememberMe);
                    return Redirect(returnUrl);
                }
                else
                {
                    //Login failure
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = userService.Register(model.Email, model.Password, model.FileName);
                if (user != null)
                {
                    UploadFile(model.ProfilePicture);
                    LoginMethod(model.Email);
                    return Redirect(returnUrl);
                }
                else
                {
                    //Login failure
                    ModelState.AddModelError(string.Empty, "Invalid register attempt.");
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        private async void UploadFile(IFormFile file)
        {
            var uploads = Path.Combine(hostingEnvironment.WebRootPath, "images/uploads");
            var filePath = Path.Combine(uploads, file.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
        }

        private async void LoginMethod(string email, bool isPersistent = false)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, email),
                new Claim(ClaimTypes.Email, email)
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = isPersistent,
            };

            await HttpContext.SignInAsync(
               CookieAuthenticationDefaults.AuthenticationScheme,
               new ClaimsPrincipal(claimsIdentity),
               authProperties);
        }
    }
}