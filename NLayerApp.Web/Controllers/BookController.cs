﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NLayerApp.Services.Common.Authors;
using NLayerApp.Services.Common.Books;
using NLayerApp.Services.Common.Books.Dto;
using NLayerApp.Services.Common.Categories;
using NLayerApp.Web.Models.Book;
using Omu.ValueInjecter;

namespace NLayerApp.Web.Controllers
{
  public class BookController : Controller
  {
    private readonly IBookService bookService;
    private readonly IAuthorService authorService;
    private readonly ICategoryService categoryService;

    public BookController(IBookService bookService, IAuthorService authorService, ICategoryService categoryService)
    {
      this.bookService = bookService;
      this.authorService = authorService;
      this.categoryService = categoryService;
    }

    [HttpGet]
    public ActionResult Index()
    {
      var detailedBookDtos = bookService.GetDetailedBooks();

      var bookModels = new List<BookModel>();
      foreach (var detailedBookDto in detailedBookDtos)
      {
        var bookModel = new BookModel();
        bookModel.InjectFrom(detailedBookDto);
        bookModels.Add(bookModel);
      }

      return View(bookModels);
    }

    [HttpGet]
    public ActionResult Details(int id)
    {
      if (id < 1) return RedirectToAction("Index"); //If the ID is negative, we redirect the user from the start and we save a trip to the database

      var bookDto = bookService.GetBookById(id);
      if (bookDto == null) return RedirectToAction("Index");

      var authorDto = authorService.GetAuthorById(bookDto.AuthorId);
      if (authorDto == null) return RedirectToAction("Index");

      var categoryDto = categoryService.GetCategoryById(bookDto.CategoryId);
      if (categoryDto == null) return RedirectToAction("Index");

      var bookModel = new BookModel
      {
        Author = authorDto.Name,
        Category = categoryDto.Name,
        IsDeleted = bookDto.IsDeleted,
        Title = bookDto.Title,
        Id = bookDto.Id
      };

      return View(bookModel);
    }

    [HttpGet]
    public ActionResult Create()
    {
      var createBookModel = new CreateBookModel();

      var authors = authorService.GetNonDeletedAuthors();
      var categories = categoryService.GetNonDeletedCategories();

      createBookModel.Authors = authors.Select(author => new SelectListItem { Value = author.Id.ToString(), Text = author.Name }).ToList();
      createBookModel.Categories = categories.Select(category => new SelectListItem { Value = category.Id.ToString(), Text = category.Name }).ToList();

      return View(createBookModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Create(CreateBookModel book)
    {
      if (!ModelState.IsValid) return View(book);

      var bookDto = new BookDto
      {
        AuthorId = book.AuthorId,
        CategoryId = book.CategoryId,
        IsDeleted = book.IsDeleted,
        Title = book.Title
      }; //Because the properties have all the same types and names, we could just use bookDto.InjectFrom(book).

      bookService.CreateBook(bookDto);
      return RedirectToAction("Index");
    }

    [HttpGet]
    public ActionResult Edit(int id)
    {
      if (id < 1) return RedirectToAction("Index"); //If the ID is negative, we redirect the user from the start and we save a trip to the database

      var bookDto = bookService.GetBookById(id);
      if (bookDto == null) return RedirectToAction("Index");

      var authors = authorService.GetNonDeletedAuthors();
      var categories = categoryService.GetNonDeletedCategories();

      var editBookModel = new EditBookModel
      {
        AuthorId = bookDto.AuthorId,
        CategoryId = bookDto.CategoryId,
        Id = bookDto.Id,
        IsDeleted = bookDto.IsDeleted,
        Title = bookDto.Title,
        Authors = authors.Select(author => new SelectListItem { Value = author.Id.ToString(), Text = author.Name }).ToList(),
        Categories = categories.Select(category => new SelectListItem { Value = category.Id.ToString(), Text = category.Name }).ToList()
      };

      return View(editBookModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(int id, EditBookModel book)
    {
      if (id < 1) return RedirectToAction("Index"); //If the ID is negative, we redirect the user from the start and we save a trip to the database

      var bookDto = bookService.GetBookById(id);
      if (bookDto == null) return RedirectToAction("Index");

      bookDto.InjectFrom(book);
      bookService.UpdateBook(bookDto);

      return RedirectToAction(nameof(Index));
    }

    [HttpGet]
    public ActionResult Delete(int id)
    {
      if (id < 1) return RedirectToAction("Index"); //If the ID is negative, we redirect the user from the start and we save a trip to the database

      var bookDto = bookService.GetBookById(id);
      if (bookDto == null) return RedirectToAction("Index");

      var authorDto = authorService.GetAuthorById(bookDto.AuthorId);
      if (authorDto == null) return RedirectToAction("Index");

      var categoryDto = categoryService.GetCategoryById(bookDto.CategoryId);
      if (categoryDto == null) return RedirectToAction("Index");

      var bookModel = new BookModel
      {
        Author = authorDto.Name,
        Category = categoryDto.Name,
        IsDeleted = bookDto.IsDeleted,
        Title = bookDto.Title,
        Id = bookDto.Id
      };

      return View(bookModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Delete(int id, IFormCollection collection)
    {
      if (id < 1) return RedirectToAction("Index"); //If the ID is negative, we redirect the user from the start and we save a trip to the database

      var bookDto = bookService.GetBookById(id);
      if (bookDto == null) return RedirectToAction("Index");

      bookDto.IsDeleted = true;
      bookService.UpdateBook(bookDto);

      return RedirectToAction(nameof(Index));
    }
  }
}