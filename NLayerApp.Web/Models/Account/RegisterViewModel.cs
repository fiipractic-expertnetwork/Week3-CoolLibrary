﻿using Microsoft.AspNetCore.Http;
using NLayerApp.Web.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NLayerApp.Web.Models.Account
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "The user need an image")]
        [DataType(DataType.Upload)]
        public IFormFile ProfilePicture { get; set; }

        [FileExtensions(Extensions = "jpg,jpeg,png", ErrorMessage = " Invalid format type")]
        public string FileName
        {
            get
            {
                if (ProfilePicture != null)
                    return ProfilePicture.FileName;
                else
                    return "";
            }
        }


        //not yett used for now
        //[Required]
        //[Display(Name = "Select country")]
        //public int CountryId { get; set; }

        //public List<CountryModel> Countries { get; set; }
    }
}
