﻿namespace NLayerApp.Web.Models.Book
{
  public class BookModel
  {
    public int Id { get; set; }
    public string Category { get; set; }
    public bool IsDeleted { get; set; }
    public string Author { get; set; }
    public string Title { get; set; }
  }
}
