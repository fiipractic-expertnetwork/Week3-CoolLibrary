﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace NLayerApp.Web.Models.Book
{
  public class CreateBookModel
  {
    [Range(0, int.MaxValue)]
    public int CategoryId { get; set; }

    public bool IsDeleted { get; set; }

    [Range(0, int.MaxValue)]
    public int AuthorId { get; set; }

    [StringLength(200)]
    public string Title { get; set; }

    public List<SelectListItem> Authors { get; set; }
    public List<SelectListItem> Categories { get; set; }
  }
}
