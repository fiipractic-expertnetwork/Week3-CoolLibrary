﻿using System.ComponentModel.DataAnnotations;

namespace NLayerApp.Web.Models.Book
{
  public class EditBookModel : CreateBookModel
  {
    [Range(0, int.MaxValue)]
    public int Id { get; set; }
  }
}
